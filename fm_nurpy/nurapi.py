from __future__ import print_function

import os
import platform
from binascii import hexlify
from ctypes import *


NUR_MAX_EPC_LENGTH = 62


class NUR_TRIGGERREAD_DATA(Structure):
    """
    C structure for tag read
    """
    _fields_ = [
        ("sensor", c_bool),
        ("source", c_int),
        ("antennaID", c_int),
        ("rssi", c_int),
        ("scaledRssi", c_int),
        ("epcLen", c_int),
        ("epc", c_byte * NUR_MAX_EPC_LENGTH),
    ]


class NUR_INVENTORY_RESPONSE(Structure):
    _fields_ = [
        ("numTagsFound", c_int),
        ("numTagsMem", c_int),
        ("roundsDone", c_int),
        ("collisions", c_int),
        ("Q", c_int),
    ]


class NUR_TAG_DATA(Structure):
    _fields_ = [
        ("timestamp", c_ushort),
        ("rssi", c_byte),
        ("scaledRssi", c_char),
        ("freq", c_uint),
        ("pc", c_ushort),
        ("channel", c_ubyte),
        ("antennaId", c_ubyte),
        ("epcLen", c_ubyte),
        ("epc", c_ubyte * NUR_MAX_EPC_LENGTH),
    ]


class NUR_TAG_DATA_EX(Structure):
    _fields_ = [
        ("timestamp", c_int),
        ("rssi", c_int),
        ("scaledRssi", c_int),
        ("freq", c_int),
        ("pc", c_int),
        ("channel", c_int),
        ("antennaId", c_int),
        ("epcLen", c_int),
        ("epc", c_byte * NUR_MAX_EPC_LENGTH),
    ]


class NurApiError(Exception):
    pass


class NurApiPingError(Exception):
    pass


class WrongKillPassword(NurApiError):
    pass


class TagByEPCNotFound(NurApiError):
    pass


class NurApi:
    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.disconnect()

    def __init__(self, lib_path=None):
        self._handle = None

        if not lib_path:
            arch = platform.machine()
            if arch == 'armv7l':
                lib_path = os.path.join(os.path.dirname(__file__),
                                        'lib', 'libNurApiRasPi.so')
            elif arch == 'x86_64':
                lib_path = os.path.join(os.path.dirname(__file__),
                                        'lib', 'libNurApix86.so')

        if not lib_path:
            raise Exception("Library not found for {}".format(arch))

        nurapi = CDLL(lib_path)

        self.nur_triggerread_data = NUR_TRIGGERREAD_DATA()

        self.NUR_SUCCESS = 0

        # enum NUR_BANK
        self.NUR_BANK_PASSWD = c_byte(0)
        self.NUR_BANK_EPC = c_byte(1)
        self.NUR_BANK_TID = c_byte(2)
        self.NUR_BANK_USER = c_byte(3)

        c_handle = c_void_p

        self._create = nurapi.NurApiCreate
        self._create.restype = c_void_p

        self._connect_serial_port = nurapi.NurApiConnectSerialPort
        self._connect_serial_port.argtypes = [c_handle, c_int, c_int]
        self._connect_serial_port.restype = c_int

        self._set_log_level = nurapi.NurApiSetLogLevel
        self._set_log_level.argtypes = [c_handle, c_int]

        self._ping = nurapi.NurApiPing
        self._ping.argtypes = [c_handle, POINTER(c_char)]
        self._ping.restype = c_int

        self._disconnect = nurapi.NurApiDisconnect
        self._disconnect.argtypes = [c_handle]
        self._disconnect.restype = c_int

        self._kill_tag_by_epc = nurapi.NurApiKillTagByEPC
        self._kill_tag_by_epc.argtypes = [
            c_handle, c_uint, POINTER(c_byte), c_uint
        ]
        self._kill_tag_by_epc.restype = c_int

        self._read_tag_by_epc = nurapi.NurApiReadTagByEPC
        self._read_tag_by_epc.argtypes = [
            c_handle,
            c_uint,
            c_bool,
            POINTER(c_byte),
            c_uint,
            c_byte,
            c_uint,
            c_int,
            c_char_p
        ]
        self._read_tag_by_epc.restype = c_int

        self._custom_read_tag_by_epc = nurapi.NurApiCustomReadTagByEPC
        self._custom_read_tag_by_epc.argtypes = [
            c_handle,
            c_uint,
            c_bool,
            POINTER(c_byte),
            c_uint,
            c_byte,
            c_uint,
            c_int,
            c_char_p
        ]
        self._custom_read_tag_by_epc.restype = c_int

        self._free = nurapi.NurApiFree
        self._free.argtypes = [c_handle]
        self._free.restype = c_int

        self._scan_single = nurapi.NurApiScanSingle
        self._scan_single.argtypes = [
            c_handle, c_int, POINTER(NUR_TRIGGERREAD_DATA)
        ]
        self._scan_single.restype = c_int

        self._get_access_password = nurapi.NurApiGetAccessPassword
        self._get_access_password.argtypes = [
            c_handle,
            c_uint,
            c_bool,
            c_byte,
            c_uint,
            c_int,
            POINTER(c_byte),
            POINTER(c_uint)
        ]
        self._get_access_password.restype = c_int

        self._write_tag_by_epc = nurapi.NurApiWriteTagByEPC
        self._write_tag_by_epc.argtypes = [
            c_handle,
            c_uint,
            c_bool,
            POINTER(c_byte),
            c_uint,
            c_byte,
            c_uint,
            c_int,
            POINTER(c_byte)
        ]
        self._write_tag_by_epc.restype = c_int

        self._get_kill_password = nurapi.NurApiGetKillPassword
        self._get_kill_password.argtypes = [
            c_handle,
            c_uint,
            c_bool,
            c_byte,
            c_uint,
            c_int,
            POINTER(c_byte),
            POINTER(c_uint)
        ]
        self._get_kill_password.restype = c_int

        self._set_kill_password = nurapi.NurApiSetKillPasswordByEPC
        self._set_kill_password.argtypes = [
            c_handle,
            c_uint,
            c_bool,
            POINTER(c_byte),
            c_uint,
            c_uint
        ]
        self._set_kill_password.restype = c_int

        self._nur_api_is_connected = nurapi.NurApiIsConnected
        self._nur_api_is_connected.argtypes = [c_handle]
        self._nur_api_is_connected.restype = c_int

        self._enable_antena = nurapi.NurApiEnablePhysicalAntenna
        self._enable_antena.argtypes = [c_handle, POINTER(c_char), c_bool]
        self._enable_antena.restype = c_int

        self._clear_tags = nurapi.NurApiClearTags
        self._clear_tags.argtypes = [c_handle]
        self._clear_tags.restype = c_int

        self._simple_inventory = nurapi.NurApiSimpleInventory
        self._simple_inventory.argtypes = [
            c_handle, POINTER(NUR_INVENTORY_RESPONSE)
        ]
        self._simple_inventory.restype = c_int

        self._fetch_tags = nurapi.NurApiFetchTags
        self._fetch_tags.argtypes = [c_handle, c_bool, c_int]
        self._fetch_tags.restype = c_int

        self._get_tag_count = nurapi.NurApiGetTagCount
        self._get_tag_count.argtypes = [c_handle, POINTER(c_int)]

        self._get_all_tag_data = nurapi.NurApiGetAllTagData
        self._get_all_tag_data.argtypes = [
            c_handle, POINTER(NUR_TAG_DATA), POINTER(c_int)
        ]

        self._get_tag_data = nurapi.NurApiGetTagData
        self._get_tag_data.argtypes = [c_handle, c_int, POINTER(NUR_TAG_DATA)]

        self._get_tag_data_ex = nurapi.NurApiGetTagDataEx
        self._get_tag_data_ex.argtypes = [
            c_handle, c_int, POINTER(NUR_TAG_DATA_EX)
        ]

        self._handle = self._create()

        self._set_log_level(self._handle, 15)

        TTY_NO = 77

        if not os.path.exists('/dev/ttyS%s' % TTY_NO):
            os.symlink('/dev/ttyACM0', '/dev/ttyS%s' % TTY_NO)

        self._connect_serial_port(self._handle, TTY_NO, 115200)

        self._enable_antena(self._handle, "ALL", True)

    @property
    def connected(self):
        return True if self._nur_api_is_connected(self._handle) == 0 else False

    def ping(self):
        ret = self._ping(self._handle, None)
        if ret != self.NUR_SUCCESS:
            raise Exception('ping failed {}'.format(ret))

    def disconnect(self):
        self._disconnect(self._handle)

    def kill_tag_by_epc(self, passwd, epc):
        epc_len = len(epc) / 2
        epc_type = c_byte * epc_len
        epc_buffer = epc_type()
        epc_bytes = []
        for i in range(0, epc_len * 2, 2):
            epc_bytes.append(int("0x" + epc[i:i + 2], 16))

        for i in range(0, len(epc_bytes)):
            epc_buffer[i] = epc_bytes[i]

        ret = self._kill_tag_by_epc(self._handle, passwd, epc_buffer, epc_len)
        if ret == 66:
            raise WrongKillPassword("Probably wrong kill password")
        elif ret == 64:
            raise TagByEPCNotFound("Tag was not found")
        if ret != self.NUR_SUCCESS:
            raise NurApiError("kill_tag_by_epc failed {}".format(ret))

    def scan_inventory(self):
        tags = []

        self._clear_tags(self._handle)

        x = NUR_INVENTORY_RESPONSE()
        self._simple_inventory(self._handle, pointer(x))

        if x.numTagsFound > 0:
            self._fetch_tags(self._handle, True, 0)

            c = c_int(0)
            self._get_tag_count(self._handle, c)

            for i in range(0, c.value):

                d = NUR_TAG_DATA()

                self._get_tag_data(self._handle, i, pointer(d))

                tags.append({
                    'epc': ''.join(["%02x" % _ for _ in d.epc[:d.epcLen]]),
                    'rssi': d.rssi,
                    'freq': d.freq,
                    'antenna_id': d.antennaId,
                })

        return tags

    def write_tag_by_epc(self, epc, data, bank=None,
                         address=0, secured=0, password=0):
        if not bank:
            bank = self.NUR_BANK_USER

        epc_len = len(epc) / 2
        epc_type = c_byte * epc_len
        epc_buffer = epc_type()
        epc_bytes = []
        for i in range(0, epc_len * 2, 2):
            epc_bytes.append(int("0x" + epc[i:i + 2], 16))

        for i in range(0, len(epc_bytes)):
            epc_buffer[i] = epc_bytes[i]

        data_hex = hexlify(data)
        data_len = len(data_hex) / 2
        data_type = c_byte * data_len
        data_buffer = data_type()
        data_bytes = []
        for i in range(0, data_len * 2, 2):
            data_bytes.append(int("0x" + data_hex[i:i + 2], 16))

        for i in range(0, len(data_bytes)):
            data_buffer[i] = data_bytes[i]

        ret = self._write_tag_by_epc(
            self._handle,
            password,
            secured,
            epc_buffer,
            epc_len,
            bank,
            address,
            data_len,
            data_buffer
        )
        if ret != self.NUR_SUCCESS:
            return False
        return True

    def set_kill_passwd_by_epc(self, epc, passwd, secured=0, password=0):

        epc_len = len(epc) / 2
        epc_type = c_byte * epc_len
        epc_buffer = epc_type()
        epc_bytes = []
        for i in range(0, epc_len * 2, 2):
            epc_bytes.append(int("0x" + epc[i:i + 2], 16))

        for i in range(0, len(epc_bytes)):
            epc_buffer[i] = epc_bytes[i]

        ret = self._set_kill_password(
            self._handle,
            password,
            secured,
            epc_buffer,
            epc_len,
            passwd
        )
        if ret != self.NUR_SUCCESS:
            return False
        return True

    def read_tag_by_epc(self, epc, bank=None, length=12,
                        address=0, secured=0, password=0):

        if not bank:
            bank = self.NUR_BANK_USER

        epc_len = len(epc) / 2

        epc_type = c_byte * epc_len
        epc_buffer = epc_type()
        epc_bytes = []
        for i in range(0, epc_len * 2, 2):
            epc_bytes.append(int("0x" + epc[i:i + 2], 16))

        for i in range(0, len(epc_bytes)):
            epc_buffer[i] = epc_bytes[i]

        read_len = length
        read_buffer = create_string_buffer(read_len)

        ret = self._read_tag_by_epc(
            self._handle,
            password,
            secured,
            epc_buffer,
            epc_len,
            bank,
            address,
            read_len,
            read_buffer
        )
        if ret != self.NUR_SUCCESS:
            return False
        return read_buffer.value
