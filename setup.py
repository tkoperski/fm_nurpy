from setuptools import setup

setup(name='fm_nurpy',
      version='0.2',
      description='Python NurconAPI Interface',
      url='https://bitbucket.org/murbanskifm/fm_nurpy',
      author='Future Mind',
      author_email='m.urbanski@futuremind.com',
      license='MIT',
      packages=['fm_nurpy'],
      include_package_data=True,
      zip_safe=False)
